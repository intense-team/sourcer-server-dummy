module.exports = (app) => {
  app.post('/api/auth/registration', (req, res) => {
    res.send({
      success: true,
    });
  });
  app.get('/api/auth/get-auth-token', (req, res) => {
    res.send({
      success: true,
      token: 'token',
    });
  });
  app.get('/api/auth/get-secret-question', (req, res) => {
    res.send({
      success: true,
      question: 'question',
    });
  });
  app.put('/api/auth/reset-password', (req, res) => {
    res.send({
      success: true,
    });
  });
  app.put('/api/auth/confirm-email', (req, res) => {
    const {
      token
    } = req.body;
    res.send({
      success: true,
    });
  });
};
