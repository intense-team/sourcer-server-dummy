module.exports = (app) => {
  app.get('/api/profile/catalog', (req, res) => {
    const {
      specializations,
      skills,
      orderBy,
    } = req.body;

    res.send({
      success: true,
      payload: {
        firstName: 'Andrey',
        middleName: 'Alekseevich',
        lastName: 'Ermakov',
        specializations: [{ id: 1, name: 'Frontend' }, {}, {}],
        skills: [{ id: 1, name: 'HTML%' }, {}],
      },
    });
  });
  app.put('/api/profile', (req, res) => {
    const {
      userName,
      firstName,
      middlName,
      lastName,
      specializations,
      skills,
    } = req.body;
    res.send({
      success: true,
      payload: {
        firstName: 'Andrey',
        middleName: 'Alekseevich',
        lastName: 'Ermakov',
        specializations: [{ id: 1, name: 'Frontend' }, {}, {}],
        skills: [{ id: 1, name: 'HTML%' }, {}],
        comments: [],
      },
    });
  });
  app.get('/api/profile/', (req, res) => {
    const {
      userID,
    } = req.body;
    res.send({
      success: true,
      payload: {
        firstName: 'Andrey',
        middleName: 'Alekseevich',
        lastName: 'Ermakov',
        specializations: [{ id: 1, name: 'Frontend' }, {}, {}],
        skills: [{ id: 1, name: 'HTML%' }, {}],
        comments: [],
      },
    });
  });
};
