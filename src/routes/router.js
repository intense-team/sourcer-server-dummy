// const userRoutes = require('./users');
// const profileRoutes = require('./profile');
const projectRoutes = require('./project');

module.exports = (app) => {
  // userRoutes(app);
  // profileRoutes(app);
  projectRoutes(app);
};
