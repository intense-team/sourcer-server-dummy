
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    name: DataTypes.STRING,
  }, {});
  Category.associate = function (models) {
    models.Category.hasMany(models.Category, {
      foreignKey: 'parentCategoryId',
    });
    models.Category.hasMany(models.Project, {
      foreignKey: 'categoryId',
    });
  };
  return Category;
};
