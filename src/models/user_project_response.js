module.exports = (sequelize, DataTypes) => sequelize.define('UserProjectResponse', {
  projectId: DataTypes.INTEGER,
  userId: DataTypes.INTEGER,
  text: DataTypes.TEXT,
});
