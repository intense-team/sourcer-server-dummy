module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    login: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    secretWord: DataTypes.STRING,
  });

  User.associate = (models) => {
    models.User.hasMany(models.Profile, {
      foreignKey: 'userId',
    });
    models.User.hasMany(models.Project, {
      foreignKey: 'userId',
    });
    models.User.hasMany(models.Project, {
      through: {
        model: models.UserProjectResponse,
        unique: false,
        otherKey: 'userId',
      },
    });
  };


  return User;
};
