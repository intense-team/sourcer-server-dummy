module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable(
      'UserProjectResponse',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        projectId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'Project',
            key: 'id',
          },
          allowNull: false,
        },
        userId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'User',
            key: 'id',
          },
          allowNull: false,
        },
        text: {
          type: Sequelize.TEXT,
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      },
    );
  },
  down(queryInterface, Sequelize) {
    return queryInterface.dropTable('UserProjectResponse');
  },
};
