const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/router.js');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

routes(app);

const port = process.env.PORT || 5000;

if (process.env.NODE_ENV !== 'test') {
  app.listen(port, () => {
    if (process.env.NODE_ENV !== 'production') {
      console.log(`app is listening on port ${port}`); // eslint-disable-line
    }
  });
}

module.exports = app; // for testing
